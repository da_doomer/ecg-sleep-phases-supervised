import numpy as np

def noise(X, factor=1.0):
    noise = (np.random.random(X.shape) * 2.0 - 1.0) * factor
    return X + noise

def destroy(X, factor=.2, idv=False):
    samples, length, nchannels = X.shape
    des_size = int(factor * length)

    Xc = np.transpose(X, (0, 2, 1))
    mask = np.ones((Xc.shape))
    for i in range(samples):
        start = np.random.randint(length)
        for j in range(nchannels):
            if idv:
                start = np.random.randint(length)
            for k in range(des_size):
                mask[i][j][(start + k) % length] = 0

    return X * np.transpose(mask, (0, 2, 1))


if __name__ == "__main__":
    X = np.random.random((5,10,4))
    # print(X)
    # print(noise(X, factor=1))
    print(destroy(X, factor=.5, idv=True))
