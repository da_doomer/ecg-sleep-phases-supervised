"""Sleep stages classifier.

This script must be executed in the folder containing a folder with the subgroup
folders with the (extracted) records of the ISRUC database (e.g.
./database/subgroup_X/3/ etc must exist)

The hyperparameters are stored in ./parameters.py

Due to problems with Pipenv you have to install tf-nightly via:
    $ pipenv install
    $ pipenv shell
    $ pip install tf-nightly
"""
# Local imports
from transforms.reporting import PDFManager
from transforms.reporting import plot_dict
from transforms.reporting import plot_matrix
from transforms.reporting import plot_confusion_matrix
from transforms.reporting import get_confusion_matrix
from transforms.reporting import plot_classification_report
from transforms.reporting import get_classification_report
from transforms.reporting import plot_bar_dict
# Model settings
from parameters import active_scaler
from parameters import active_models
from parameters import build_model
from parameters import channel_scaler_factory
# Dataset settings
from parameters import active_dataset_name
from parameters import shuffle_split
from parameters import build_dataset
from parameters import rec_ids
from parameters import frac_val
from parameters import QUICK

# Python imports
from random import random, shuffle
from statistics import mean
from collections import Counter
from collections import defaultdict
from collections import namedtuple
import sys
import traceback
import numpy as np

# Profiling
from memory_profiler import profile as profile_mem
from profilehooks import profile as profile_time

from time import time

from pathlib import Path

# Data augmentation
import data_aug as da

import gc

# Where to plot train results
if QUICK: result_pdf = PDFManager("quick.pdf")
else:     result_pdf = PDFManager("results.pdf")

def eval_hist(h0,h1):
    return mean((int(h0i==h1i) for h0i,h1i in zip(h0,h1)))

def test(model, x, y, x_val, y_val, test_data):
    if len(test_data) == 0: raise ValueError("Nothing to test with! (len(test_data)==0)")
    n_train = len(x)
    n_val = len(x_val)

    start_time = time()
    history = model.fit(x,y, validation_data=(x_val,y_val))
    history = {"{} {}".format(str(key), str(model)) : vals for key,vals in history.items()}
    end_time = time()
    training_time = end_time-start_time

    if len(history.items()) > 0:
        print("Training history history")
        print(history)
        f = plot_dict(history)
        result_pdf.add_fig(f)

    # Evaluate histogram building on unseen recordings
    accuracies = list()
    x_test, y_test, y_test_pred = list(), list(), list()
    for x_t,hypnograms in test_data:
        hypnos_to_plot = dict()
        model_hypnogram = model.predict(x_t)
        title = '{} ({} agreement with expert 0)'.format(
            str(model),"{0:.2f}".format(eval_hist(hypnograms[0],model_hypnogram)))
        hypnos_to_plot[title] = model_hypnogram
        x_test.extend(x_t)
        y_test.extend(hypnograms[0])
        y_test_pred.extend(model_hypnogram)
        for i,hypnogram in enumerate(hypnograms):
            # Calculate test_acc (model_hypnogram vs hypnogram)
            accuracies.append(eval_hist(hypnogram, model_hypnogram))
            # Compare expert_i with expert_0
            title = 'expert {} ({} agreement with expert 0)'.format(
                            i,"{0:.2f}".format(eval_hist(hypnograms[0],hypnograms[i])))
            hypnos_to_plot[title] = hypnogram
        f = plot_dict(hypnos_to_plot)
        result_pdf.add_fig(f)

    title = "{} test".format(str(model))
    plotters = [plot_confusion_matrix, plot_classification_report]
    for plotter in plotters:
        f = plotter(y_test,y_test_pred,title=title)
        result_pdf.add_fig(f)

    ## Validate model
    x_test = np.array(x_test)
    y_test = np.array(y_test)
    performance = {
            "acc":model.evaluate(x,y),
            "test_acc":model.evaluate(x_test,y_test),
            "test_acc_avg_experts":mean(accuracies),
            "training_time":training_time,
    }

    print("Final accuracy on hypnogram building over {} unseen records:\
            {}".format(len(test_data),mean(accuracies)))

    return performance, y_test, y_test_pred

def leave_one_out(noise=False, destroy=False):
    leave_one_out_performance = defaultdict(lambda : defaultdict(list))
    confusion_matrices = defaultdict(list)
    classification_reports = defaultdict(list)
    class_rep_ticks = (None, None)
    for i in range(len(rec_ids)):
        # Leave i out
        test_ids = [rec_ids[i]]
        train_ids = rec_ids[:i] + rec_ids[i+1:]
        print("Leave-one-out {} of {}".format(i+1,len(rec_ids)))
        print("test",test_ids)
        print("train",train_ids)

        # Check if dataset exists
        try:
            x, y = build_dataset(train_ids[:1])
        except Exception as e:
            print("Dataset not found! Check that ./database/subgroupX/Y/ exists")
            raise e
        # Check if all models build
        classes = set(list(y))
        x_shape = x.shape[1:]
        n_classes = len(classes)
        print(x.shape)
        for ac in active_models:
            print(str(ac))
            build_model(x_shape, n_classes, ac)

        # Split testing and training folders
        # Build dataset
        x, y, x_val, y_val = shuffle_split(*build_dataset(train_ids),frac_val=0.10)
        print(Counter(y))

        # Normalize
        scaler = active_scaler(x.shape[2], channel_scaler_factory)
        #scaler.fit(x)
        #x = scaler.transform(x)
        #x_val = scaler.transform(x_val)

        # Build test data
        test_data = list()
        augmentation_label = str() # Augmentation data identifier for report
        for rec_id in test_ids:
            x_test, hypnograms = build_dataset([rec_id],multi_hypnogram=True)
            #x_test = scaler.transform(x_test)
            if noise != False:
                test_data.append([da.noise(x_test, factor=noise), hypnograms])
                augmentation_label = " noise {}".format(str(noise))
            elif destroy != False:
                test_data.append([da.destroy(x_test, factor=destroy), hypnograms])
                augmentation_label = " destroy {}".format(str(destroy))
            else:
                test_data.append([x_test, hypnograms])

        # Build model
        model_errors = list()
        performance_summary = defaultdict(dict)
        def model_instances():
            for ac in active_models:
                yield build_model(x_shape, n_classes, ac)
        for model in model_instances():
            print(str(model))
            try:
                performance, y_test, y_test_pred = test(model,x,y,x_val,y_val,test_data)
                # Add performance data to the dict
                for metric,values in performance.items():
                    key1 = "{}".format(str(metric))
                    # If data augmentation, insert in metric string.
                    key1 = key1 + augmentation_label
                    key2 = "{}".format(str(model))
                    performance_summary[key1][key2] = values
                model_conf_m = get_confusion_matrix(y_test,y_test_pred)
                model_class_rep, xt, yt = get_classification_report(y_test,y_test_pred, ticks=True)
                confusion_matrices[str(model)].append(model_conf_m)
                classification_reports[str(model)].append(model_class_rep)
                class_rep_ticks = (xt,yt)

                del model
                del y_test
                del y_test_pred
                del performance
                gc.collect()

            except Exception as e:
                model_errors.append("{} on {}".format(str(e), str(model)))
                e = sys.exc_info()[0]
                print(e)
                traceback.print_exc()

        print("Errors")
        print(model_errors)

        for metric in performance_summary.keys():
            for model in performance_summary[metric].keys():
                value = performance_summary[metric][model]
                leave_one_out_performance[metric][model].append(value)

    # Average leave-one-out performances
    leave_one_out_avgs = defaultdict(dict)
    for metric in leave_one_out_performance.keys():
        for model in leave_one_out_performance[metric].keys():
            values = leave_one_out_performance[metric][model]
            leave_one_out_avgs[metric][model] = mean(values)

    print(leave_one_out_performance)
    for metric, metricdict in leave_one_out_avgs.items():
        title='LOO {}\non {}'.format(str(metric), str(active_dataset_name))
        f = plot_bar_dict(metricdict, title=title)
        result_pdf.add_fig(f)

    # Average leave-one-out confusion matrices and classification reports
    for model in classification_reports.keys():
        title = "LOO {} avg\non {}".format(str(model),str(active_dataset_name))

        avg_classification_report = np.mean(classification_reports[model],axis=0)
        xticks=class_rep_ticks[0]
        yticks=class_rep_ticks[1]
        f = plot_matrix(avg_classification_report, xticks, yticks, title)

        avg_confusion_matrix = np.mean(confusion_matrices[model],axis=0)
        xticks=["{} (true)".format(c) for c in classes]
        yticks=["{} (pred)".format(c) for c in classes]
        f = plot_matrix(avg_confusion_matrix, xticks, yticks, title)

if __name__ == "__main__":
    leave_one_out()
    leave_one_out(noise=0.2)
    leave_one_out(destroy=0.2)
