"""Specifies the functions to build the (x,y) =(epoch_wavelets[][], epochs)
dataset, including ECG segmentation."""
# Internal imports
from transforms.datasets.isruc_opener import open_recording

# Python imports
from glob import glob
from itertools import chain
from multiprocessing import Pool
from statistics import median

# External imports
from sklearn.preprocessing import minmax_scale
from tqdm import tqdm
from scipy.signal import resample
import numpy
from hyperdim.utils import to_categorical
from biosppy.signals.ecg import ecg as biosppy_ecg_processing

def build_dataset(rec_folder, target_len):
    """Returns numpy arrays,

    epochs[i][j][k] = the kth value in time of the jth wavelet in the
    ith epoch.

    stages[i] = the stage at the ith epoch.

    assert len(epochs) == len(stages)

    Returns: epochs[][][], stages[]"""
    wavelets_per_epoch = 19
    # Open recording
    signals, hypnogram, sample_rate = open_recording(rec_folder,["X2"])
    ecg = signals[0]
    # Split in 30 second epochs
    w = sample_rate * 30
    ecg_epochs = [ecg[i:i + w] for i in range(0, len(ecg), w)]
    assert len(ecg_epochs) == len(hypnogram)
    # Segment ECG for each epoch
    epochs = numpy.zeros((len(hypnogram), wavelets_per_epoch, target_len))
    # Resample wavelets to target_len size
    for i in range(len(hypnogram)):
        _,_,_,_,wavelets,_,_ = biosppy_ecg_processing(ecg_epochs[i], sampling_rate = sample_rate, show=False)
        for j in range(wavelets_per_epoch):
            wavelet = resample(wavelets[j], target_len)
            for k in range(target_len):
                epochs[i][j][k] = wavelet[k]

    rec_stages = hypnogram
    assert len(epochs) == len(rec_stages)
    return (epochs, rec_stages)
