"""Defines the function to open ISRUC database folders"""
import pyedflib
from glob import glob
from scipy.signal import detrend
import numpy
from sklearn.preprocessing import minmax_scale
import sklearn.preprocessing
from scipy.interpolate import CubicSpline

def resample(signal, target_len):
    if len(signal) == target_len: return signal
    xold = minmax_scale(range(len(signal)), feature_range=(0,1))
    xnew = minmax_scale(range(target_len), feature_range=(0,1))
    yold = signal

    ynew = CubicSpline(xold, yold)(xnew)
    return ynew

def open_recording(rec_id, channels, multi_hypnogram = True):
    """Opens rec_folder from the Sleep-Acel (Apple watch) database

    sample_rate = max(sample rates of the channels)

    assert len(signals[i]) == len(signals[j])

    The dataset only has two channels, "heartrate" and "motion".

    The dataset only has one hypnogram per recording.

    Returns signals[], hypnogram, sample_rate"""
    rec_files = 
    # Read channels
    signals_raw    = list()
    sample_rates   = list()
    signals_labels = list()
    with pyedflib.EdfReader(rec_file) as f:
        heads = f.getSignalHeaders()
        labels = [header["label"] for header in heads]
        rates = [header["sample_rate"] for header in heads]
        for i,label in enumerate(labels):
            if label in channels:
                signals_raw.append(f.readSignal(i))
                signals_labels.append(label)
                sample_rates.append(rates[i])
    #if len(signals_labels) == 1:
    #    print(labels)
    #    print(signals_labels)
    #    print(channels)

    # Resample channels
    target_len = min([len(signal) for signal in signals_raw])
    sample_rate = min(sample_rates)
    signals = list()
    for i,signal in enumerate(signals_raw):
        if len(signal) != target_len:
            print("Resampling {} from {}Hz to {}Hz".format(
                  signals_labels[i],sample_rates[i],sample_rate))
            signal = resample(signal, target_len)
        signals.append(signal)

    # Read hypnogram
    hypno_files = glob(rec_folder + "/*.txt")
    hypnograms = list()
    for hypno_file in hypno_files:
        with open(hypno_file) as h:
            h = [line for line in h if len(line.strip()) > 0 ]
            h = list(map(int,h))
            fixer = lambda n: 4 if n == 5 else n
            h = [fixer(n) for n in h]
            hypnogram = h
            hypnograms.append(hypnogram)
    h1, h2 = hypnograms

    # Remove first and last epochs
    # The authors ignore the last 30 epochs in their extraction
    # https://sleeptight.isr.uc.pt/ISRUC_Sleep/?page_id=76
    samples = numpy.array(signals).T
    w = sample_rate * 30
    epochs = [samples[i:i + w] for i in range(0, len(samples), w)]

    # Depending on the sample rate, there may not be enough complete epochs
    h1 = h1[:len(epochs)]
    h2 = h2[:len(epochs)]

    left_trim = 10
    right_trim = -40
    epochs = epochs[left_trim:right_trim]
    h1 = h1[left_trim:right_trim]
    h2 = h2[left_trim:right_trim]

    ## Remove ambigous epochs
    #unambigous_epochs = list()
    #for h1i, h2i, epochi in zip(h1,h2,epochs):
    #    if h1i == h2i:
    #        unambigous_epochs.append(epochi)
    #epochs = unambigous_epochs

    #samples = [samples for epoch in epochs for samples in epoch]
    samples = numpy.concatenate(epochs)
    signals = numpy.array(samples).T
    #for i in range(len(signals)):
    #    detrend(signals, axis=i, overwrite_data=True)

    hypnograms = [h1, h2]
    for h in hypnograms:
        assert len(h) == len(epochs)

    return signals, hypnograms, sample_rate
