"""Specifies the functions to build diferent datasets from the ISRUC database.

single_chanel_dataset()
the single-channel epoch dataset.
That is, each sample is a 30 seconds epoch of a single channel.
(x,y) = samples,stages

"""
# Internal imports
from transforms.datasets.isruc_opener import open_recording

# Python imports
from glob import glob
from itertools import chain
from multiprocessing import Pool
from statistics import median

# External imports
import pyedflib
from sklearn.preprocessing import minmax_scale
from tqdm import tqdm
from scipy.signal import resample
import numpy as np

def build_dataset(rec_folder, channels, multi_hypnogram=False):
    """Returns np arrays,

    epochs[i][j][k] = the kth value of the jth channel at the ith epoch.
    stages[i] = the stage at the ith epoch.

    assert len(epochs[i]) == len(stages)

    Returns: epochs[][][], stages[]"""
    # Open recording
    signals, hypnograms, sample_rate = open_recording(rec_folder, channels)
    # Transpose from (channels,time) to (time,channels)
    samples = np.array(signals).T
    # Split in 30 second epochs
    w = sample_rate * 30
    epochs = [samples[i:i + w] for i in range(0, len(samples), w)]
    epochs = np.array(epochs)
    # Add time to all channels for all epochs
    n_channels = epochs.shape[1]
    epochs = [np.append(epoch,[i]*n_channels) for i,epoch in enumerate(epochs)]
    epochs = np.array(epochs)

    assert len(hypnograms[0]) == len(epochs)
    if not multi_hypnogram: y = hypnograms[0]
    else: y = hypnograms
    return (epochs, y)
