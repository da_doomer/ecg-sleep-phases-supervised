"""Specifies the functions to build diferent datasets from the ISRUC database.

single_chanel_dataset()
the single-channel epoch dataset.
That is, each sample is a 30 seconds epoch of a single channel.
(x,y) = samples,stages

"""
# Python imports
from glob import glob
from itertools import chain
from multiprocessing import Pool
from statistics import median

# External imports
import pyedflib
from sklearn.preprocessing import minmax_scale
from tqdm import tqdm
from scipy.signal import resample
import numpy

from parameters import Sources
from transforms.datasets.isruc_opener import open_recording as isruc_opener
from transforms.datasets.edf_opener import open_recording as edf_opener

def build_dataset(rec_folder, channels, source, multi_hypnogram=False):
    """Returns numpy arrays,

    epochs[i][j][k] = the kth value of the jth channel at the ith epoch.
    stages[i] = the stage at the ith epoch.

    assert len(epochs[i]) == len(stages)

    Returns: epochs[][][], stages[]"""
    if source == Sources.isruc_epoch_multichannel:
        open_recording = isruc_opener
    elif source == Sources.edf_epoch_multichannel:
        open_recording = edf_opener
    else:
        raise ValueError("Dataset builder not defined for source {}".format(str(source)))
    # Open recording
    signals, hypnograms, sample_rate = open_recording(rec_folder, channels,
            multi_hypnogram=True)
    # Transpose from (channels,time) to (time,channels)
    samples = numpy.array(signals).T
    # Split in 30 second epochs
    w = sample_rate * 30
    epochs = [samples[i:i + w] for i in range(0, len(samples), w)]
    epochs = numpy.array(epochs)

    assert len(hypnograms[0]) == len(epochs)
    if not multi_hypnogram: y = hypnograms[0]
    else: y = hypnograms
    return (epochs, y)
