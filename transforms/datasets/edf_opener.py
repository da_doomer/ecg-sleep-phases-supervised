"""Defines the function to open ISRUC database folders"""
import pyedflib
from glob import glob
from scipy.signal import detrend
import numpy
import numpy as np
from sklearn.preprocessing import minmax_scale
import sklearn.preprocessing
from scipy.interpolate import CubicSpline

def resample(signal, target_len):
    if len(signal) == target_len: return signal
    xold = minmax_scale(range(len(signal)), feature_range=(1,1))
    xnew = minmax_scale(range(target_len), feature_range=(1,1))
    yold = signal

    ynew = CubicSpline(xold, yold)(xnew)
    return ynew

from multiprocessing import Pool
from multiprocessing import Lock
def workf(li): return li[0],li[1],li[2](li[3])
def open_recording(rec_id, channels, multi_hypnogram = True):
    """Opens rec_folder from the ISRUC database

    sample_rate = max(sample rates of the channels)

    assert len(signals[i]) == len(signals[j])

    Subgroup III has channels:
    ['LOC-A2', 'ROC-A1', 'F3-A2', 'C3-A2', 'O1-A2', 'F4-A1', 'C4-A1', 'O2-A1',
    'X1', 'X2', 'X3', 'X4', 'X5', 'X6', 'DC3', 'X7', 'X8', 'SaO2', 'DC8']

    Subgroup I has channels:
    ['E1-M2', 'E2-M1', 'F3-M2', 'C3-M2', 'O1-M2', 'F4-M1', 'C4-M1', 'O2-M1',
    'X1', 'X2', 'X3', 'X4', 'X5', 'X6', 'DC4', 'X7', 'X8', 'SpO2', 'DC8', 'DC7']

    Returns signals[], hypnogram, sample_rate"""
    # Read channels
    rec_file = glob(rec_id + "*-PSG.edf")[0]
    signals_raw    = list()
    sample_rates   = list()
    signals_labels = list()
    try:
        with pyedflib.EdfReader(rec_file) as f:
            heads = f.getSignalHeaders()
            labels = [header["label"] for header in heads]
            rates = [header["sample_rate"] for header in heads]
            work = list()
            for i,label in enumerate(labels):
                if label in channels:
                    work.append((label,rates[i],f.readSignal,i))
            for l,r,s in map(workf,work):
                signals_raw.append(s)
                signals_labels.append(l)
                sample_rates.append(r)
    except Exception as e:
        print("Error on {}".format(rec_file))
        raise e
    #if len(signals_labels) == 1:
    #    print(labels)
    #    print(signals_labels)
    #    print(channels)

    # Resample channels
    if len(sample_rates) == 0:
        print("Warning! Record {} has labels {} but none of the requested {}".format(
            rec_file,str(labels),str(channels)))
        return [],[],0
    target_len = min([len(signal) for signal in signals_raw])
    sample_rate = min(sample_rates)

    signals = list()
    for i,signal in enumerate(signals_raw):
        assert len(signal) == target_len
        signals.append(signal)

    # Read hypnogram
    hypno_file = glob(rec_id + "*-Hypnogram.edf")[0]
    h1 = list()
    try:
        with pyedflib.EdfReader(hypno_file) as f:
            for start_s, duration_s, stage in zip(*f.readAnnotations()):
                assert duration_s % 30 == 0
                h1.extend([stage]*int(duration_s//30))
    except Exception as e:
        print("Error on {}".format(rec_file))
        raise e
    # Use standard compression to 5 class problem
    mapping = {
            "Sleep stage W":0,
            "Sleep stage 1":1,
            "Sleep stage 2":2,
            "Sleep stage 3":3,
            "Sleep stage 4":3,
            "Sleep stage R":4,
            "Sleep stage ?":-1,
            "Movement time":-1,
            }
    h1 = [mapping[s] for s in h1]

    samples = numpy.array(signals).T
    w = sample_rate * 30
    epochs = [samples[i:i + w] for i in range(0, len(samples), w)]

    epochs = np.array(epochs)
    h1 = np.array(h1)

    # there may not be enough complete epochs
    if len(epochs) != len(h1):
        print("Warning: Record {} has {} PSG complete epochs but only {} "
        "epochs in its hypnogram! Will trim to the shortest"
        "sequence.".format(rec_id,len(epochs),len(h1)))
        target = min(map(len,[h1,epochs]))
        h1 = h1[:target]
        epochs = epochs[:target]

    # As is standard remove "Sleep stage ?" and "movement time" epochs
    h1_clean = list()
    epochs_clean = list()
    for i in range(len(epochs)):
        if h1[i] != -1:
            h1_clean.append(h1[i])
            epochs_clean.append(epochs[i])
    epochs = np.array(epochs_clean)
    h1 = np.array(h1_clean)

    samples = numpy.concatenate(epochs)
    signals = numpy.array(samples).T
    #for i in range(len(signals)):
    #    detrend(signals, axis=i, overwrite_data=True)

    hypnograms = [h1]
    for h in hypnograms:
        assert len(h) == len(epochs)
    if multi_hypnogram:
        return signals, hypnograms, sample_rate
    else:
        return signals, h1, sample_rate
