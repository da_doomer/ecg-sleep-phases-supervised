"""Defines the function to open ISRUC database folders"""
import pyedflib
from glob import glob
from scipy.signal import detrend
import numpy
from sklearn.preprocessing import minmax_scale
import sklearn.preprocessing
from scipy.interpolate import CubicSpline

def resample(signal, target_len):
    if len(signal) == target_len: return signal
    xold = minmax_scale(range(len(signal)), feature_range=(1,1))
    xnew = minmax_scale(range(target_len), feature_range=(1,1))
    yold = signal

    ynew = CubicSpline(xold, yold)(xnew)
    return ynew

from multiprocessing import Pool
from multiprocessing import Lock
def workf(li): return li[0],li[1],li[2](li[3])
def open_recording(rec_folder, channels, multi_hypnogram = True):
    """Opens rec_folder from the ISRUC database

    sample_rate = max(sample rates of the channels)

    assert len(signals[i]) == len(signals[j])

    Subgroup III has channels:
    ['LOC-A2', 'ROC-A1', 'F3-A2', 'C3-A2', 'O1-A2', 'F4-A1', 'C4-A1', 'O2-A1',
    'X1', 'X2', 'X3', 'X4', 'X5', 'X6', 'DC3', 'X7', 'X8', 'SaO2', 'DC8']

    Subgroup I has channels:
    ['E1-M2', 'E2-M1', 'F3-M2', 'C3-M2', 'O1-M2', 'F4-M1', 'C4-M1', 'O2-M1',
    'X1', 'X2', 'X3', 'X4', 'X5', 'X6', 'DC4', 'X7', 'X8', 'SpO2', 'DC8', 'DC7']

    Returns signals[], hypnogram, sample_rate"""
    # Read channels
    rec_file = glob(rec_folder + "/*.rec")[0]
    signals_raw    = list()
    sample_rates   = list()
    signals_labels = list()
    with pyedflib.EdfReader(rec_file) as f:
        heads = f.getSignalHeaders()
        labels = [header["label"] for header in heads]
        rates = [header["sample_rate"] for header in heads]
        work = list()
        for i,label in enumerate(labels):
            if label in channels:
                work.append((label,rates[i],f.readSignal,i))
        for l,r,s in map(workf,work):
            signals_raw.append(s)
            signals_labels.append(l)
            sample_rates.append(r)
    #if len(signals_labels) == 1:
    #    print(labels)
    #    print(signals_labels)
    #    print(channels)

    # Resample channels
    if len(sample_rates) == 0:
        print("Warning! Record {} has labels {} but none of the requested {}".format(
            rec_folder,str(labels),str(channels)))
        return [],[],0
    target_len = min([len(signal) for signal in signals_raw])
    sample_rate = min(sample_rates)
    if len(set(sample_rates)) > 1: print("Resampling to {}Hz!".format(sample_rate))
    signals = list()
    for i,signal in enumerate(signals_raw):
        if len(signal) != target_len:
            print("Resampling {} from {}Hz to {}Hz".format(
                  signals_labels[i],sample_rates[i],sample_rate))
            signal = resample(signal, target_len)
        signals.append(signal)

    # Read hypnogram
    hypno_files = glob(rec_folder + "/*.txt")
    hypnograms = list()
    for hypno_file in hypno_files:
        with open(hypno_file) as h:
            h = [line for line in h if len(line.strip()) > 0 ]
            h = list(map(int,h))
            fixer = lambda n: 4 if n == 5 else n
            h = [fixer(n) for n in h]
            hypnogram = h
            hypnograms.append(hypnogram)
    h1, h2 = hypnograms

    # Remove first and last epochs
    # The authors ignore the last 30 epochs in their extraction
    # https://sleeptight.isr.uc.pt/ISRUC_Sleep/?page_id=76
    samples = numpy.array(signals).T
    w = sample_rate * 30
    epochs = [samples[i:i + w] for i in range(0, len(samples), w)]

    # Depending on the sample rate, there may not be enough complete epochs
    h1 = h1[:len(epochs)]
    h2 = h2[:len(epochs)]

    left_trim = 10
    right_trim = -30
    epochs = epochs[left_trim:right_trim]
    h1 = h1[left_trim:right_trim]
    h2 = h2[left_trim:right_trim]

    ## Remove ambigous epochs
    #unambigous_epochs = list()
    #for h1i, h2i, epochi in zip(h1,h2,epochs):
    #    if h1i == h2i:
    #        unambigous_epochs.append(epochi)
    #epochs = unambigous_epochs

    #samples = [samples for epoch in epochs for samples in epoch]
    samples = numpy.concatenate(epochs)
    signals = numpy.array(samples).T
    #for i in range(len(signals)):
    #    detrend(signals, axis=i, overwrite_data=True)

    hypnograms = [h1, h2]
    for h in hypnograms:
        assert len(h) == len(epochs)
    if multi_hypnogram:
        return signals, hypnograms, sample_rate
    else:
        return signals, h1, sample_rate
