from sklearn.utils import class_weight
from sklearn.utils.multiclass import unique_labels
from PyPDF2 import PdfFileMerger
from os import remove, rename
from os.path import isfile
import numpy as np

from threading import RLock
from pathlib import PurePath

from parameters import cmap
class PDFManager:
    """Thread safe representation of a PDF file"""
    def __init__(self, pdf_dest):
        pdf_dest = pdf_dest
        self.pdf_dest = pdf_dest
        self.lock = RLock()

    def add_pdf(self, pdf, bookmark = None):
        """Appends pdf to pdf_dest. If the file
        did not exist it is created."""
        with self.lock:
            pdf_dest = self.pdf_dest
            filename3 = "./temp3.pdf"
            merger = PdfFileMerger()
            if isfile(pdf_dest): merger.append(pdf_dest)
            if bookmark is not None: merger.append(pdf, bookmark = bookmark)
            else: merger.append(pdf)
            merger.write(filename3)
            merger.close()
            rename(filename3, pdf_dest)

    def add_fig(self, f, show = False):
        """Convinience function. Adds the plot f to the pdf"""
        with self.lock:
            fig_pdf = "./fig.pdf"
            f.tight_layout()
            f.savefig(fig_pdf)
            self.add_pdf(fig_pdf, self.pdf_dest)
            remove(fig_pdf)
            f.clear()
            plt.close("all")

import matplotlib.pyplot as plt

from sklearn.metrics import confusion_matrix

import matplotlib
def heatmap(data, row_labels, col_labels, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-90, ha="right")

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["black", "white"],
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A list or array of two color specifications.  The first is used for
        values below a threshold, the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts

def plot_matrix(matrix, x_ticks, y_ticks, title):
    f, ax = plt.subplots()
    ax.set_title(title)
    im, cbar = heatmap(matrix, x_ticks, y_ticks, ax=ax,
                   cmap=cmap)
    texts = annotate_heatmap(im, valfmt="{x:.2f}")
    return f

from sklearn.metrics import classification_report
def get_classification_report(y, y_pred,ticks=False):
    y, y_pred = np.array(y), np.array(y_pred)
    # Only plot labels with all metrics
    cr_dict = classification_report(y, y_pred,output_dict = True)
    #print(classification_report(y, y_pred))
    metrics = set()
    supports = dict()
    for key in cr_dict:
        if isinstance(cr_dict[key],dict):
            supports[key] = cr_dict[key].pop('support',0)
            for metric in cr_dict[key]:
                    metrics.add(str(metric))
    rows  = [key for key, val in cr_dict.items()
            if isinstance(val,dict)
            and set(val.keys()) == metrics
            ]
    columns = list(metrics)
    x_ticks = list(rows)
    y_ticks = list(columns)
    matrix = np.empty((len(x_ticks),len(y_ticks)))
    for i,ik in enumerate(x_ticks):
        for j,jk in enumerate(y_ticks):
            matrix[i][j] = cr_dict[ik][jk]
    if ticks: return matrix,x_ticks,y_ticks
    return matrix

def plot_classification_report(y, y_pred, title = str()):
    y, y_pred = np.array(y), np.array(y_pred)
    # Only plot labels with all metrics
    cr_dict = classification_report(y, y_pred,output_dict = True)
    print(classification_report(y, y_pred))
    #print(classification_report(y, y_pred))
    metrics = set()
    supports = dict()
    for key in cr_dict:
        if isinstance(cr_dict[key],dict):
            supports[key] = cr_dict[key].pop('support',0)
            for metric in cr_dict[key]:
                    metrics.add(str(metric))
    rows  = [key for key, val in cr_dict.items()
            if isinstance(val,dict)
            and set(val.keys()) == metrics
            ]
    columns = list(metrics)
    x_ticks = list(rows)
    y_ticks = list(columns)
    matrix = np.empty((len(x_ticks),len(y_ticks)))
    for i,ik in enumerate(x_ticks):
        for j,jk in enumerate(y_ticks):
            matrix[i][j] = cr_dict[ik][jk]

    # Plot metrics and supports
    f, ax = plt.subplots(nrows=1,ncols=2)
    ax[0].set_title(title)
    im, cbar = heatmap(matrix, x_ticks, y_ticks, ax=ax[0],
                   cmap=cmap)
    texts = annotate_heatmap(im, valfmt="{x:.2f}")
    matrix = np.array([[float(i)] for i in supports.values()])
    xticks = supports.keys()
    im, cbar = heatmap(matrix, x_ticks, ['support'], ax=ax[1],
                   cmap=cmap)
    texts = annotate_heatmap(im, valfmt="{x:.0f}")
    return f

def get_confusion_matrix(y_true, y_pred,
        normalize=True):
    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = list(map(str,set(y_true)|set(y_pred)))
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    return cm

def plot_confusion_matrix(y_true, y_pred,
                          normalize=True,
                          title=str(),
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = list(map(str,set(y_true)|set(y_pred)))
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    xticks=["{} (true)".format(c) for c in classes]
    yticks=["{} (pred)".format(c) for c in classes]
    f = plot_matrix(cm, xticks, yticks, title)
    return f

def plot_dict(dic : dict):
    f, axs = plt.subplots(nrows=len(dic.items()), squeeze=False)
    axs = axs.flatten()
    for i,(label, values) in enumerate(dic.items()):
        #label = label + "\n{}".format(str(values))
        axs[i].set_title(label)
        axs[i].plot(values)
    return f

def plot_bar_dict(dic : dict, title : str = str()):
    print(title)
    print(dic)
    f = plt.figure()
    plt.title(title)
    dic = {k+"\n"+str(dic[k]):dic[k] for k in sorted(dic,key=lambda k:dic[k])}
    colors = cmap(np.linspace(0.25, 0.75, len(dic)))
    plt.barh(*zip(*dic.items()), color=colors)
    return f
