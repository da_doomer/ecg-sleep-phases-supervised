import os

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.models import Model
# https://keras.io/layers/core/#repeatvector
from tensorflow.keras.layers import Layer
from tensorflow.keras.layers import RepeatVector
from tensorflow.keras.layers import Reshape
# https://en.wikipedia.org/wiki/Convolutional_neural_network#Pooling
from tensorflow.keras.layers import MaxPooling1D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import AveragePooling2D
from tensorflow.keras.layers import AveragePooling1D
from tensorflow.keras.layers import GlobalAveragePooling2D
from tensorflow.keras.layers import GlobalAveragePooling1D
from tensorflow.keras.layers import LocallyConnected2D
from tensorflow.keras.layers import UpSampling2D
from tensorflow.keras.layers import Dense
# https://www.cs.toronto.edu/~hinton/absps/JMLRdropout.pdf
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
# https://en.wikipedia.org/wiki/Convolutional_neural_network#Convolutional_layer
from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import BatchNormalization
# https://keras.io/layers/merge/
from tensorflow.keras.layers import concatenate
from tensorflow.keras.layers import Concatenate
from tensorflow.keras.layers import Lambda
# https://keras.io/layers/noise#GaussianNoise
from tensorflow.keras.layers import GaussianNoise
# https://en.wikipedia.org/wiki/Convolutional_neural_network#ReLU_layer
from tensorflow.keras.layers import LeakyReLU
# https://keras.io/optimizers/
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.optimizers import Adamax
from tensorflow.keras.optimizers import Nadam
from tensorflow.keras.optimizers import Adadelta
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.utils import plot_model
from tensorflow.keras.utils import to_categorical as keras_cat
from numpy import argmax

#print(model.output_shape)

from hyperdim.hdmodel import HDModel

import numpy as np
from functools import reduce

from collections import namedtuple

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RandomizedSearchCV
from sklearn.utils import class_weight

from typing import Dict

from abc import ABC
from abc import abstractmethod

def to_categorical(l):
    return keras_cat(l)

class BaseModel(ABC):
    def __init__(self, model):
        self.model = model
    @abstractmethod
    def fit(self,x,y,validation_data=None) -> Dict[str,list]: return{}
    @abstractmethod
    def predict(self,x) -> list: return [0]*len(x)
    @abstractmethod
    def evaluate(self,x,y) -> float: 0.0
    def summary(self): print(str(self))
    def __str__(self): return str(type(self).__name__)

from collections import Counter
from random import sample
from tensorflow.keras.callbacks import EarlyStopping
from datetime import datetime
from tensorflow.keras.callbacks import TensorBoard
from random import randint
from random import shuffle

def balanced_epoch_generator(x,y,epoch_size):
    """Categorical y"""
    hoty = [int(e) for e in y.argmax(1)]
    classes_i = {c : [i for i,yc in enumerate(hoty) if yc==c] for c in set(hoty)}
    epoch_x = np.empty((epoch_size,*x.shape[1:]))
    epoch_y = np.empty((epoch_size,*y.shape[1:]))
    epoch_indexes = list()
    b = 0
    while True:
        epoch_indexes.clear()
        keys = list(classes_i.keys())
        c = 0
        while len(epoch_indexes) < epoch_size:
            c = (c+1)%len(keys)
            epoch_indexes.extend(sample(classes_i[c],1))
        shuffle(epoch_indexes)
        for bi,oi in enumerate(epoch_indexes):
            epoch_x[bi][:] = x[oi][:]
            epoch_y[bi][:] = y[oi][:]
        hottest = [int(e) for e in epoch_y.argmax(1)]
        yield epoch_x, epoch_y
        b+=1

import gc
from collections import Counter
def fitkeras(keras,x,y,validation_data,kwargs):
    """Categorical y"""
    args = dict(kwargs)

    # Set callbacks
    early_stopping = EarlyStopping(monitor='accuracy', patience=70)
    early_stopping2 = EarlyStopping(monitor='val_accuracy', patience=70)
    log_dir="./logs/fit/{}/{}-id{}".format(
            datetime.now().strftime("%Y%m%d-%H%M%S"),
            str(keras),
            randint(0,20))
    tensorboard_callback = TensorBoard(log_dir=log_dir, histogram_freq=1)
    args["callbacks"]=[
            early_stopping,
            early_stopping2,
            #tensorboard_callback,
        ]

    # Set generator
    batch_size = args['batch_size']
    del args['validation_split']
    epochs = args['epochs']
    args['shuffle']=False

    # Set validation data
    if validation_data:
        x_val, y_val = validation_data
        if len(y_val) > 0:
            args['validation_data'] = (x_val,y_val)

    # Half epochs with unbalanced dataset
    args['epochs']=epochs
    hoty = [int(e) for e in y.argmax(1)]
    weights = class_weight.compute_class_weight('balanced',np.unique(hoty),hoty)
    args['class_weight']=weights
    args['shuffle']=True
    history = keras.fit(x,y,**args)

    # Half epochs with balanced dataset
    #for i,(a,b) in enumerate(balanced_epoch_generator(x,y,epoch_size=len(x))):
    #    if i == epochs//4: break
    #    print("Balanced training {}".format(i))
    #    args['epochs']=epochs//4+i+1
    #    args['initial_epoch']=epochs//4+i
    #    args['shuffle']=False
    #    history = keras.fit(a, b, **args)
    #    gc.collect()
    return history.history


class KerasModel(BaseModel):
    def __init__(self, model, fit_kwargs):
        model = Model(name=str(self),inputs=model.input, outputs=model.output)
        optimizer = Adam()
        model.compile(loss='categorical_crossentropy',
                metrics=['accuracy'],
                optimizer=optimizer)
        model.summary()
        try:
            plot_model(model, show_shapes=True, show_layer_names=True, to_file="{}.pdf".format(str(self)))
        except: pass
        super().__init__(model)
        # Setup tensorboard callbacks
        self.fit_kwargs = fit_kwargs

    def fit(self, x, y, validation_data=None):
        y = to_categorical(y)
        if validation_data:
            xv,yv = validation_data
            if len(yv) > 0: yv = to_categorical(yv)
            validation_data = (xv,yv)
        return fitkeras(self.model,x,y,validation_data,self.fit_kwargs)

    def predict(self,x): return argmax(self.model.predict(x), axis=-1)
    # keras.model.evaluate returns: [loss,acc]
    def evaluate(self,x,y): return self.model.evaluate(x,to_categorical(y),verbose =0)[1]
    def plot(self, f): plot_model(self.model, to_file = f, show_shapes=True)
    def summary(self): self.model.summary()

# Input transformers
class InputTransformerModel(ABC):
    """Abstract class for models that transform their inputs before fitting."""
    @abstractmethod
    def transform(self, x): pass
    def fit(self,x,y,**kwargs):
        x = self.transform(x)
        return super().fit(x,y,**kwargs)
    def predict(self,x,**kwargs):
        x = self.transform(x)
        return super().predict(x,**kwargs)
    def evaluate(self,x,y,**kwargs):
        x = self.transform(x)
        return super().evaluate(x,y,**kwargs)

class SKLearnModel(BaseModel): # Will search the best parameters
    def __init__(self, init_kwargs=dict(), fit_kwargs=dict(), search_kwargs=dict()):
        self.init_kwargs = init_kwargs
        self.fit_kwargs = fit_kwargs
        model = self.build_model()
        super().__init__(model)
    def build_model(self): pass # e.g. return DecisionTree
    def flatten_x(self,x): return np.reshape(x,(len(x), -1))
    def fit(self, x, y, validation_data=None):
        x = self.flatten_x(x)
        self.model.fit(x,y,**self.fit_kwargs)
        return {}
    def predict(self,x):
        x = self.flatten_x(x)
        return self.model.predict(x)
    def evaluate(self,x,y):
        x = self.flatten_x(x)
        return self.model.score(x,y)

from multiprocessing import Pool
class ChannelScaler:
    """Expects x.shape = (samples_n, channels_len, channels_n).
    Keeps an scaler for each channel.
    """
    def __init__(self,n_channels, scaler_factory):
        self.scalers = [scaler_factory() for _ in range(n_channels)]
    def flatten_channels(self,x):
        """(samples_n, channels_len, channels_n) -> (channels_n,-1)"""
        samples_n   = x.shape[0]
        channel_len = x.shape[1]
        channels_n  = x.shape[2]
        new_x = np.empty((channels_n,samples_n*channel_len),x.dtype)
        #def assign(j1,j2,i):
        #    global x
        #    global new_x
        #    new_x[i][j1*channel_len+j2] = x[j1][j2][i]
        #with Pool() as p:
        #    c = samples_n*channel_len*channels_n//(p._processes*4)
        #    p.map(lambda ji,v: assign(*ji,v), np.ndindex(x.shape), chunksize=c)
        for (j1,j2,i), value in np.ndenumerate(x):
            new_x[i][j1*channel_len+j2] = value
        return new_x
    def fit(self,x):
        """Expects x.shape = (samples_n, channels_len, channels_n)"""
        for scaler,channel in zip(self.scalers,self.flatten_channels(x)):
            scaler.fit(channel[:,np.newaxis])
        #zipped = zip(self.scalers,self.flatten_channels(x))
        #with Pool() as p:
        #    p.map(lambda s,c: s.fit(c[:,np.newaxis]), zipped)
    def transform(self,x):
        """(samples_n, channels_len, channels_n) -> same shape"""
        samples_n   = x.shape[0]
        channel_len = x.shape[1]
        channels_n  = x.shape[2]
        new_x = np.empty((samples_n,channel_len, channels_n))
        # (samples_n, channels_n, channels_len)
        for i, sample in enumerate(x):
            for k, channel in enumerate(sample.T):
                for j, val in enumerate(self.scalers[k].transform(channel[:,np.newaxis])):
                    new_x[i,j,k] = val
        return new_x

class RandomizedSearcher(SKLearnModel): # Will search the best parameters
    def __init__(self, init_kwargs=dict(), fit_kwargs=dict(), search_kwargs=dict()):
        self.init_kwargs = init_kwargs
        self.fit_kwargs = fit_kwargs
        model = self.build_model()
        random_search = RandomizedSearchCV(model,**search_kwargs)
        self.model = random_search

class SimpleCNN2D(KerasModel):
    """Made for x_shape = (n,m,) or (n,m,1) or (n,m,1,1) or etc.
    Based on
    https://keras.io/examples/cifar10_cnn/
    """
    def __init__(self, x_shape, classes, fit_kwargs):
        model = Sequential()
        n = x_shape[0]
        m = x_shape[1]
        model.add(Reshape((n,m,1,), input_shape=x_shape))
        ## Transformación inicial de la serie
        model.add(Conv2D(128, (5, 5), padding='same', input_shape = x_shape))
        model.add(Activation('relu'))

        # Aprende una representación (identifica lugares importantes)
        model.add(Conv2D(256, (5, 5), padding='same'))
        model.add(Activation('relu'))
        model.add(Conv2D(256, (5, 5)))
        model.add(Activation('relu'))
        model.add(AveragePooling2D(pool_size=2))
        model.add(Dropout(0.25))

        model.add(Conv2D(256, (5, 5), padding='same'))
        model.add(Activation('relu'))
        model.add(Conv2D(256, (5, 5)))
        model.add(Activation('relu'))
        model.add(AveragePooling2D(pool_size=2))
        model.add(Dropout(0.25))

        # Ajusta la representación aprendida para clasificar
        model.add(Flatten())
        model.add(Dense(32))
        model.add(Activation('relu'))
        model.add(Dropout(0.5))
        model.add(Dense(classes))
        model.add(Activation('softmax'))

        super().__init__(model, fit_kwargs)

class DensenetModel(KerasModel):
    """
    growth_rate: number of filters for the convolutional layers
    n_layers_block: convolutional layers in each dense block

    In the original paper:
    k = growth_rate
    L = total_layers = n_dense_blocks*n_layers_block

    Example values in the paper:
    k = 12
    L = 100

    Based on:
    https://arxiv.org/pdf/1608.06993v3.pdf
    https://github.com/TheAILearner/Densely-Connected-Convolutional-Networks/blob/master/DenseNet.py
    """
    def dense_block(self,
            inputs,
            current_filters,
            growth_rate,
            n_layers_block):
        for i in range(n_layers_block):
           each_layer = self.conv_layer(inputs, growth_rate)
           inputs = concatenate([inputs, each_layer])
           current_filters += growth_rate
        output_filters = current_filters
        outputs = inputs
        return outputs, output_filters

    @abstractmethod
    def conv_layer(self, inputs, filters): pass
    @abstractmethod
    def transition_layer(self, inputs, filters): pass

class Densenet2D(DensenetModel):
    """Made for x_shape = (n,m,) or (n,m,1,) or (n,m,1,1) or etc.
    E.g. 128,128,1
    """

    def __init__(self,x_shape, classes, fit_kwargs,
        growth_rate=12,
        n_dense_blocks=4,
        n_layers_block=3):
        # Keras functional model
        n = x_shape[0]
        m = x_shape[1]
        input_img = Input(shape=x_shape)
        inputs = Reshape((n,m,1))(input_img)
        inputs = Conv2D(24, (3, 3), kernel_initializer='he_uniform', padding='same', use_bias=False)(inputs)
        inputs = BatchNormalization()(inputs)
        inputs = Activation('relu')(inputs)

        inputs = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(inputs)
        filters = growth_rate
        transition_filters = growth_rate
        for _ in range(n_dense_blocks - 1):
            inputs, filters = self.dense_block(inputs, filters, growth_rate, n_layers_block)
            inputs, filters = self.transition_layer(inputs, transition_filters)

        inputs, filters = self.dense_block(inputs, filters, growth_rate, n_layers_block)
        inputs = BatchNormalization()(inputs)
        inputs = Activation('relu')(inputs)
        inputs = GlobalAveragePooling2D()(inputs)

        output = Dense(classes, activation='softmax', name="output")(inputs)

        model = Model(input_img, output)

        optimizer = Adam()
        super().__init__(model, fit_kwargs)

    def conv_layer(self, inputs, filters):
        inputs = BatchNormalization()(inputs)
        inputs = Activation('relu')(inputs)
        inputs = Conv2D(filters, (3, 3),\
                        kernel_initializer='he_uniform',\
                        padding='same',\
                        use_bias=False)(inputs)
        inputs = Activation('relu')(inputs)
        inputs = Dropout(0.2)(inputs)
        outputs = inputs
        return outputs

    def transition_layer(self, inputs, filters):
        inputs = BatchNormalization()(inputs)
        inputs = Activation('relu')(inputs)
        inputs = Conv2D(filters, (1, 1),\
                        kernel_initializer='he_uniform', padding='same',\
                        use_bias=False)(inputs)
        inputs = AveragePooling2D((2, 2), strides=(2, 2))(inputs)
        output_filters = filters
        outputs = inputs
        return outputs, output_filters

def _pseudo_square(n, m):
    import math
    def divisors(n):
        divs = [1]
        for i in range(2,int(math.sqrt(n))+1):
            if n%i == 0:
                divs.extend([i,n//i])
                divs.extend([n])
        return list(set(divs))
    divs = divisors(n)
    difs = [abs(n//div - m*div) for div in divs]
    choice = difs.index(min(difs))
    new_n = n//divs[choice]
    new_m = m*divs[choice]
    return new_n, new_m


import math
from math import ceil
import numpy as np
class Pseudosquaring(Layer):
    # TODO use stacking and padding
    # TODO include batch size in description
    # input  = (steps, channels)
    # output = (n,     m,       stack_size)
    # stacking = n (assuming steps, channels) = (s,c):
    # o[0,0] =(i[0][0], i[1][0], ..., i[n][0])
    # o[1,0] =(i[0][1], i[1][0], ..., i[n][0])
    # ...
    # o[c,0]   =(i[0][c], i[1][0], ..., i[n][0])
    # o[0,1] =(i[1][0], i[1][0], ..., i[n][0])
    # o[1,1] =(i[1][1], i[1][0], ..., i[n][0])
    # ...
    # o[c,1]   =(i[1][c], i[1][0], ..., i[n][0])
    # ...
    # ...
    # If cropping, no need to add extra values
    # else, go backwards when reaching the end, then forwards, etc.
    known_shapes = dict()
    def __init__(self, max_stack_size, crop=True, **kwargs):
        super().__init__(**kwargs)
        self.max_stack_size = max_stack_size
        if crop != True:
            raise ValueError("crop = False not valid!")
        self.crop = crop

    def compute_output_shape(self, input_shape):
        steps = input_shape[1]
        channels = input_shape[2]
        input_shape = tuple(input_shape.as_list())
        if input_shape in self.known_shapes.keys():
            return self.known_shapes[input_shape]
        possible_shapes = set()
        for i in [k*channels for k in range(1,steps*channels)]:
            for k in range(1,self.max_stack_size):
                j = (steps*channels)/(i*k)
                if not min((i,j,k)) >= 1: continue
                i,j,k = map(ceil,(i,j,k))
                if not self.crop or i*j*k <= steps*channels:
                    possible_shapes.add((i,j,k))
        o_shape = min(possible_shapes, key = lambda s: abs(s[0]-s[1]))
        self.known_shapes[input_shape] = (input_shape[0],*o_shape)
        return (input_shape[0],*o_shape)

    def call(self, inputs):
        o_shape = self.compute_output_shape(inputs.shape)
        channels = inputs.shape[2]
        def _pseudo_square(tensor):
            RES, TRAN = 0, 1
            t_shape = o_shape[1:]
            program = [
                    [RES,*t_shape],
                    [RES, t_shape[0]//channels, t_shape[1]*t_shape[2], channels],
                    [TRAN,0,2,1],
                    [RES,*t_shape]
            ]
            for instruction in program:
                operation = instruction[0]
                operands  = instruction[1:]
                if operation == RES:
                    tensor = tf.reshape(tensor, shape = operands)
                elif operation == TRAN:
                    tensor = tf.transpose(tensor, perm = operands)
                else:
                    raise ValueError("Operation not supported: {}".format(operation))
            return tensor
        return tf.stack(tf.map_fn(_pseudo_square, inputs))

class Densenet15D(DensenetModel):
    """Made for x_shape = (n,m,) or (n,m,1,) or (n,m,1,1) or etc.

    n is expected to be big (~6000) and m to be small (<20) (e.g. 6000,1,1)

    Pseudosquares.
    """
    def __init__(self, x_shape, classes, fit_kwargs,
            growth_rate=12,
            n_dense_blocks=4,
            n_layers_block=3):
        # Keras functional model
        n = x_shape[0]
        m = x_shape[1]
        input_img = Input(shape=x_shape)

        inputs = Pseudosquaring(max_stack_size=10)(input_img)
        #new_n, new_m = _pseudo_square(n,m)
        #inputs = Reshape((new_n,new_m,1))(input_img)

        inputs = Conv2D(24, (3, 3), kernel_initializer='he_uniform', padding='same', use_bias=False)(inputs)

        inputs = BatchNormalization()(inputs)
        inputs = Activation('relu')(inputs)

        inputs = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(inputs)
        filters = growth_rate
        transition_filters = growth_rate
        for _ in range(n_dense_blocks - 1):
            inputs, filters = self.dense_block(inputs, filters, growth_rate, n_layers_block)
            inputs, filters = self.transition_layer(inputs, transition_filters)

        inputs, _ = self.dense_block(inputs, filters, growth_rate, n_layers_block)
        inputs = BatchNormalization(name="target")(inputs)
        inputs = Activation('relu')(inputs)
        inputs = GlobalAveragePooling2D()(inputs)

        output = Dense(classes, activation='softmax', name="output")(inputs)

        model = Model(input_img, output)

        optimizer = Adam()
        super().__init__(model, fit_kwargs)

    def conv_layer(self, inputs, filters):
        inputs = BatchNormalization()(inputs)
        inputs = Activation('relu')(inputs)
        inputs = Conv2D(filters, (3, 3),\
                        kernel_initializer='he_uniform',\
                        padding='same',\
                        use_bias=False)(inputs)
        inputs = Dropout(0.2)(inputs)
        outputs = inputs
        return outputs

    def transition_layer(self, inputs, filters):
        inputs = BatchNormalization()(inputs)
        inputs = Activation('relu')(inputs)
        inputs = Conv2D(filters, (1, 1),\
                        kernel_initializer='he_uniform', padding='same',\
                        use_bias=False)(inputs)
        inputs = AveragePooling2D((2, 2), strides=(2, 2))(inputs)
        output_filters = filters
        outputs = inputs
        return outputs, output_filters

class Experiment(KerasModel):
    """Made for x_shape = (n,m,) or (n,m,1,) or (n,m,1,1) or etc.

    n is expected to be big (~6000) and m to be small (<20) (e.g. 6000,1,1)

    m 1D independent densenets
    """
    def __init__(self, x_shape, classes, fit_kwargs):
        # Transform (n,m,1,1,...) -> (n,m)
        n = x_shape[0]
        m = x_shape[1]
        input_img = Input(shape=x_shape)
        inputs = Reshape((n,m))(input_img)

        inputs = Conv1D(20, kernel_size=200, strides=1,
                activation='relu')(inputs)
        inputs = MaxPooling1D(pool_size=20, strides=10)(inputs)
        n1 = inputs.shape.as_list()[1]
        n2 = inputs.shape.as_list()[2]
        inputs = Reshape((n1,n2,1,))(inputs)
        inputs = Conv2D(400, kernel_size=(30, 20), strides=(1,1),
                activation='relu')(inputs)
        n1 = inputs.shape.as_list()[1]
        n2 = inputs.shape.as_list()[3]
        inputs = Reshape((n1,n2,))(inputs)
        inputs = MaxPooling1D(pool_size=10, strides=2)(inputs)
        inputs = MaxPooling1D(pool_size=10, strides=2)(inputs)
        inputs = Flatten(name="target")(inputs)
        inputs = Dense(128, activation="relu")(inputs)
        inputs = Dropout(0.6)(inputs)
        inputs = Dense(1024, activation="relu")(inputs)
        inputs = Dropout(0.6)(inputs)
        inputs = Dense(8192, activation="relu")(inputs)
        inputs = Dropout(0.6)(inputs)
        inputs = Dense(1024, activation="relu")(inputs)
        inputs = Dropout(0.6)(inputs)
        inputs = Dense(128, activation="relu")(inputs)
        inputs = Dropout(0.6)(inputs)

        inputs = Flatten()(inputs)
        output = Dense(classes, activation='softmax',name="output")(inputs)
        model = Model(input_img, output)

        optimizer = Adam()
        super().__init__(model, fit_kwargs)

class SpecializedDensenet1D(DensenetModel):
    """Made for x_shape = (n,m,) or (n,m,1,) or (n,m,1,1) or etc.

    n is expected to be big (~6000) and m to be small (<20) (e.g. 6000,1,1)

    m 1D independent densenets
    """
    def __init__(self, x_shape, classes, fit_kwargs,
            growth_rate=12,
            n_dense_blocks=4,
            n_layers_block=3,
            n_branches=None):
        # Transform (n,m,1,1,...) -> (n,m)
        n = x_shape[0]
        m = x_shape[1]
        input_img = Input(shape=x_shape)
        inputs = Reshape((n,m))(input_img)
        inputs = BatchNormalization(name="target")(inputs)

        # Branch into m single-channel 1D layers
        # (n,m) -> [(n,1),...,(n,1)]
        branches = list()
        for i in range(m):
            out = Lambda(lambda x,i:
                    x[...,i],output_shape=(n,),arguments={'i':i})(inputs)
            out = Reshape((n,1))(out)
            branches.append(out)

        # Branch branches
        actual_branches = list()
        for branch in branches:
            inputs = branch
            for i in range(n_branches):
                actual_branches.append(inputs)
        branches = actual_branches

        # Append a sequence of 1D denseblocks and transitions
        # to each branch
        leafs = list()
        for branch in branches:
            filters = growth_rate
            transition_filters = growth_rate
            inputs = branch
            for _ in range(n_dense_blocks - 1):
                inputs, filters = self.dense_block(inputs,
                        filters,
                        growth_rate,
                        n_layers_block)
                inputs, filters = self.transition_layer(inputs,
                        transition_filters)
            inputs, filters = self.transition_layer(inputs,
                    transition_filters)
            inputs = Flatten()(inputs)
            inputs = Dense(64)(inputs)
            inputs = Dense(64)(inputs)
            leafs.append(inputs)

        # Concatenate all the sub densenets
        if len(leafs) > 1: inputs = concatenate(leafs)

        inputs = BatchNormalization(name="target")(inputs)
        inputs = Activation('relu')(inputs)
        inputs = Flatten()(inputs)

        output = Dense(classes, activation='softmax',name="output")(inputs)
        model = Model(input_img, output)

        optimizer = Adam()
        super().__init__(model, fit_kwargs)

    def conv_layer(self, inputs, filters):
        inputs = BatchNormalization()(inputs)
        inputs = Activation('relu')(inputs)
        inputs = Conv1D(filters,
                kernel_size = 128,
                strides=1,
                kernel_initializer='he_uniform',
                padding='same',
                use_bias=False)(inputs)
        inputs = Dropout(0.2)(inputs)
        outputs = inputs
        return outputs

    def transition_layer(self, inputs, filters):
        inputs = BatchNormalization()(inputs)
        inputs = Activation('relu')(inputs)
        inputs = Conv1D(filters,
                kernel_size=64,
                strides = 1,
                kernel_initializer='he_uniform',
                padding='same',
                use_bias=False)(inputs)
        inputs = AveragePooling1D(pool_size=32,
                strides = 1,
                )(inputs)
        output_filters = filters
        outputs = inputs
        return outputs, output_filters

class SpecializedDensenet15D(DensenetModel):
    """Made for x_shape = (n,m,) or (n,m,1,) or (n,m,1,1) or etc.

    n is expected to be big (~6000) and m to be small (<20) (e.g. 6000,1,1) ie
    (channel_len, channel_n)

    One 1.5Densenet for each class if branches = None
    """
    def __init__(self, x_shape, classes, fit_kwargs,
            growth_rate=12,
            n_dense_blocks=4,
            n_layers_block=3,
            n_branches=None):
        # Transform (n,m,1,1,...) -> (n,m)
        n = x_shape[0]
        m = x_shape[1]
        #window_size = (m,m*10)
        window_size = (m*4,1)
        inputs = Input(shape=x_shape)
        input_img = inputs
        inputs = BatchNormalization()(inputs)
        #inputs = Pseudosquaring(max_stack_size=10)(inputs)
        #new_n, new_m = _pseudo_square(n,m)
        inputs = Reshape((n,1,m))(inputs)

        # Branch into classes single-channel 1D layers
        # (n,m) -> [(n,1),...,(n,1)]
        branches = list()
        if n_branches is None: n_branches = classes
        for i in range(n_branches):
            branches.append(inputs)

        leafs = list()
        hdout = list()
        for branch in branches:
            inputs = Conv2D(growth_rate, window_size, kernel_initializer='he_uniform', padding='same')(branch)
            inputs = Activation('relu')(inputs)
            inputs = MaxPooling2D(window_size, strides=window_size, padding='same')(inputs)
            filters = growth_rate
            transition_filters = growth_rate
            for _ in range(n_dense_blocks - 1):
                inputs, filters = self.dense_block(inputs, filters, growth_rate, n_layers_block,window_size)
                inputs, filters = self.transition_layer(inputs, transition_filters,window_size)
            #inputs, _ = self.dense_block(inputs, filters, growth_rate, n_layers_block, window_size)
            inputs = Conv2D(1,
                            kernel_size = window_size,\
                            kernel_initializer='he_uniform',\
                            padding='same',\
                            #implementation=2,\
                            )(inputs)
            #inputs = AveragePooling2D()(inputs)
            inputs = Activation('sigmoid')(inputs)
            inputs = Flatten()(inputs)
            inputs = Dense(10000//len(branches), activation="sigmoid")(inputs)
            leafs.append(inputs)

        # Concatenate all the sub densenets (post global pooling)
        if len(leafs) > 1: inputs = concatenate(leafs)

        inputs = Flatten()(inputs)
        inputs = BatchNormalization()(inputs)
        #for _ in range(4):
        #    inputs = Dense(128, activation="sigmoid")(inputs)
        # Setup out layer for HD (but ignore)
        #hdout = concatenate(hdout, name = "target")
        inputs = Flatten(name="target")(inputs)
        #inputs = Activation("sigmoid", name="target")(inputs)
        #inputs = Dense(classes, activation='sigmoid')(inputs)
        output = Dense(classes, activation='softmax', kernel_constraint=tf.keras.constraints.UnitNorm(axis=0))(inputs)
        model = Model(input_img, output)
        super().__init__(model, fit_kwargs)

    def dense_block(self,
            inputs,
            current_filters,
            growth_rate,
            n_layers_block,
            window_size):
        for i in range(n_layers_block):
           each_layer = self.conv_layer(inputs, growth_rate, window_size)
           inputs = concatenate([inputs, each_layer])
           current_filters += growth_rate
        output_filters = current_filters
        outputs = inputs
        return outputs, output_filters

    def conv_layer(self, inputs, filters, window_size):
        inputs = BatchNormalization()(inputs)
        inputs = Activation('relu')(inputs)
        inputs = Conv2D(filters,
                        kernel_size = window_size,\
                        kernel_initializer='he_uniform',\
                        padding='same',\
                        )(inputs)
        inputs = Dropout(0.2)(inputs)
        outputs = inputs
        return outputs

    def transition_layer(self, inputs, filters, window_size):
        inputs = BatchNormalization()(inputs)
        inputs = Activation('relu')(inputs)
        inputs = Conv2D(filters, 
                        kernel_size = window_size,\
                        kernel_initializer='he_uniform', padding='same',\
                        use_bias=False)(inputs)
        inputs = AveragePooling2D(window_size, strides=window_size)(inputs)
        output_filters = filters
        outputs = inputs
        return outputs, output_filters

class TsinalisCNN(KerasModel):
    """Made for x_shape = (n,) or (n,m,) or (n,m,1,) ...
    with small m (e.g. 6000,3)

    Based on https://arxiv.org/pdf/1610.01683.pdf
    """
    def __init__(self,x_shape, classes, fit_kwargs):
        model = Sequential()
        n = x_shape[0]
        if len(x_shape) == 1: m = 1
        else:                 m = x_shape[1]
        model.add(Reshape((n,m,), input_shape=x_shape))

        model.add(Conv1D(20, kernel_size=200, strides=1, activation='relu'))
        model.add(MaxPooling1D(pool_size=20, strides=10))
        n1 = model.layers[-1].output_shape[1]
        n2 = model.layers[-1].output_shape[2]
        model.add(Reshape((n1,n2,1,)))
        model.add(Conv2D(400, kernel_size=(30, 20), strides=(1,1), activation='relu'))
        n1 = model.layers[-1].output_shape[1]
        n2 = model.layers[-1].output_shape[3]
        model.add(Reshape((n1,n2,)))
        #model.add(MaxPooling1D(pool_size=10, strides=2))
        model.add(MaxPooling1D(pool_size=10, strides=2))
        model.add(Flatten(name="target"))
        model.add(Dense(500, activation="relu"))
        model.add(Dense(500, activation="relu"))
        model.add(Dense(classes, activation="softmax", name="output"))

        super().__init__(model, fit_kwargs)

from functools import reduce
class HD(InputTransformerModel):
    def __init__(self,x_shape, classes):
        features = reduce(lambda x,y: x*y, x_shape)
        model = HDModel(features, classes)
        super().__init__(model)
    def transform(self, x): return x.flatten()


from sklearn.preprocessing import minmax_scale
class ANNEncoder:
    """Use the output of a Keras model as encoder.

    The encoding process for a sample is equivalent to using the
    flattened ann output for that sample.

    If the ann has output shape `(None,x1,x2,...)`, then `self.d` will be
    `x1*x2*x3*...` (will flatten).

    Parameters
    ----------
    ann : Keras model
        Model whose output will be used to encode samples.

    layer_name : str
        Name of the keras model layer

    """
    def __init__(self, ann, layer_name):
        ann_cut     = Model(inputs=ann.input, outputs=ann.get_layer(name=layer_name).output)
        dims        = ann_cut.get_layer(index=-1).output_shape[1:]
        self.d      = reduce(lambda x,y: x*y, dims)
        # Build intermediate ann as encoder
        self.ann   = ann_cut

    def encode(self, samples, verbose = 0, **kwargs):
        """Encodes the samples using the ann.

        Parameters
        ----------
        samples : iterable
            Data to encode on. Must have shape compatible with
            (len(x), self.features)

        verbose : int, {0, 1}
            If 0 no messages will be printed.

        **kwargs : dict
            Arguments that will be passed to ann.predict()

        Returns
        -------
        encoded_samples: iterable
            The encoded samples with shape `(len(samples, self.d)` as
            described above.
        """
        predictions = self.ann.predict(samples)
        encoded_samples = list()
        for prediction in predictions:
            encoded_samples.append(prediction.flatten())
        #encoded_samples = np.array(encoded_samples)
        #encoded_samples = minmax_scale(encoded_samples,feature_range=(-1,1),axis=0)
        return np.array(encoded_samples)

class ANNHD(BaseModel, ABC):
    """Build a composite Keras-HDModel with fit(), predict() and summary()
    methods.

    The Keras ann will be fit() and then the output of layer_name will be used
    as encoder for the HDModel.

    The encoding process for a sample is equivalent to using the
    flattened ann layer output for that sample.

    Will build a model compatible with
        input_shape  = ann.get_layer(0).input_shape[1]
        output_shape = ann.get_layer(-1).output_shape[1]

    It is assumed that:

        ann has output (None,classes)
        ann gives ann.get_layer(name=layer_name)

    If:
        layer_name has output shape (None,x1,x2,...), then self.hd will have
        d=x1 x2 x3 (will flatten)

    """
    @abstractmethod
    def build_ann(self, x_shape, classes, init_kwargs): pass
    def __init__(self,
            x_shape,
            classes,
            init_kwargs,
            fit_kwargs,
            layer_name="target"):

        self.ann = self.build_ann(x_shape, classes, init_kwargs)
        self.fit_kwargs = fit_kwargs

        # Build Encoder (split ann) and HDModel
        encoder = ANNEncoder(self.ann, layer_name)
        # HD y_shape is the number of classes
        hd_y_shape = self.ann.get_layer(index=-1).output_shape[1]
        # HD x_shape is the input of the ann
        hd_x_shape = self.ann.get_layer(index=0).input_shape[1:]
        self.hd = HDModel(hd_x_shape, hd_y_shape, d=encoder.d, encoder=encoder)

        super().__init__(self.hd)

    def fit(self, x, y, validation_data=None):
        """Splits the data and fits on the ann using kwargs, then on hd,
        additional parameters go to the anns fit."""
        y = to_categorical(y)
        verbose = False
        if validation_data is not None:
            x_val, y_val = validation_data
            if len(y_val) > 0: y_val = to_categorical(y_val)
        else: x_val, y_val = [],[]


        if verbose: print("Fit ANN")
        if len(y_val) > 0:
            val = (x_val,y_val)
        else:
            val = None
        history = fitkeras(self.ann,x,y,val,self.fit_kwargs)

        ## Trim x and y to feature the same size for all classes
        #from random import sample
        #def xyize(xys):
        #    return map(np.array,([d[0] for d in xys], [d[1] for d in xys]))
        #data = [(x[i],y[i]) for i in range(len(x))]
        #eval_y = list(np.argmax(y, axis=1))
        #max_elems = min([eval_y.count(yi) for yi in set(eval_y)])
        #balanced_data = list()
        #for cls in set(eval_y):
        #    data_cls = [data[i] for i in range(len(data)) if eval_y[i]==cls]
        #    balanced_data.extend(sample(data_cls, max_elems))
        #x, y = xyize(balanced_data)

        if verbose: print("One shot fit HD")
        ## Train with small batches
        #batch_size = 32
        #for i,(a,b) in enumerate(balanced_epoch_generator(x,y,epoch_size=batch_size)):
        #    if i == len(x)//batch_size: break
        #    print("Balanced training {}/{}".format(i,len(x)//batch_size))
        #    hist2 = self.hd.fit(a,b,verbose=verbose)
        #    gc.collect()
        #    # Extend the history object
        #    hd_acc = hist2["acc"][-1]
        #    history["accuracy"].append(hd_acc)

        hist2 = self.hd.fit(x,y,verbose=verbose)
        hd_acc = hist2["acc"][-1]
        history["accuracy"].append(hd_acc)

        if verbose: print("HD acc {}".format(hd_acc))

        return history

    def predict(self,x):
        return argmax(self.hd.predict(x), axis=-1)
    def evaluate(self, x, y):
        y = to_categorical(y)
        return self.hd.evaluate(x,y)
    def summary(self):
        self.ann.summary()
        print("coupled to HDModel of dimensionality d = {}".format(self.hd.d))

class SpecializedDensenet1DHD(ANNHD):
    def build_ann(self, x_shape, classes, init_kwargs):
        return SpecializedDensenet1D(x_shape, classes, fit_kwargs={}, **init_kwargs).model

class Densenet15DHD(ANNHD):
    def build_ann(self, x_shape, classes, init_kwargs):
        return Densenet15D(x_shape, classes, fit_kwargs={}, **init_kwargs).model

class SpecializedDensenet15DHD(ANNHD):
    def build_ann(self, x_shape, classes, init_kwargs):
        return SpecializedDensenet15D(x_shape, classes, fit_kwargs={}, **init_kwargs).model

class DecisionTree(RandomizedSearcher):
#class DecisionTree(SKLearnModel):
    def build_model(self):
        return DecisionTreeClassifier(**self.init_kwargs)

class RandomForest(SKLearnModel):
    def build_model(self):
        return RandomForestClassifier(**self.init_kwargs)

# FFT
from scipy.fftpack import fft as fft
import matplotlib.pyplot as plt
class FFTTransformer(InputTransformerModel):
    """InputTransformerModel to apply FFT.
    """
    def transform(self, x):
        nx = np.empty_like(x)
        for i in range(len(x)):
            res = list()
            for channel in x[i].T:
                transformed = np.real(fft(channel))
                res.append(transformed)
            nx[i] = np.array(res).T
        return nx

from pywt import dwt
class DWTTransformer(InputTransformerModel):
    """InputTransformerModel to apply FFT."""
    def transform(self, x):
        nx = np.empty_like(x)
        for i in range(len(x)):
            res = list()
            for channel in x[i].T:
                cA,cD = dwt(channel, "db1")
                transformed = np.concatenate((cA,cD))[:len(channel)]
                res.append(transformed)
            nx[i] = np.array(res).T
        return nx

class TsinalisFFT(FFTTransformer, TsinalisCNN):
    pass

class SpecializedDensenet1DHDFFT(FFTTransformer, SpecializedDensenet1DHD):
    pass

class Densenet15DHDFFT(FFTTransformer, Densenet15DHD):
    pass

class Densenet15DHDDWT(DWTTransformer, Densenet15DHD):
    pass

class SpecializedDensenet15DHDFFT(FFTTransformer, SpecializedDensenet15DHD):
    pass

class SpecializedDensenet15DHDDWT(DWTTransformer, SpecializedDensenet15DHD):
    pass

class RahmanForestDWT(DWTTransformer, RandomForest):
    pass

# Voting system
from sklearn.preprocessing import KBinsDiscretizer
class VotingClassifier(BaseModel, ABC):
    # TODO
    def fit(self, x, y):
        return self.discretizer.transform(x)
