from transforms import datasets
from transforms import models
from transforms.datasets import isruc_opener
from transforms.datasets import edf_opener

# External imports
import scipy.stats
import sklearn.preprocessing
import numpy as np
import matplotlib.pyplot as plt

# Python imports
from collections import namedtuple
from multiprocessing import Pool
from tqdm import tqdm
from pathlib import Path

## MAIN SETTINGS:
cmap = plt.cm.PuBu
# dataset structure
from enum import Enum, auto
class Sources(Enum):
        isruc_epoch_wavelets = auto()
        isruc_epoch_multichannel =  auto()
        isruc_epoch_multichannel_timed = auto()
        sleepacel_epoch_multichannel =  auto()
        edf_epoch_multichannel =  auto()
active_dataset = Sources.isruc_epoch_multichannel
# Number of recordings to use

# target channels epoch_multichannel
# (same channel, inconsistent names in group I)
# ID       subI      subIII
if active_dataset == Sources.isruc_epoch_multichannel:
    EOG_L  = ('LOC-A2', 'E1-M2', 'LOC')
    EOG_R  = ('ROC-A1', 'E2-M1', 'ROC')
    EEG_1  = ('F3-A2',  'F3-M2')
    EEG_2  = ('C3-A2',  'C3-M2', 'C3')
    EEG_3  = ('O1-A2',  'O1-M2')
    EEG_4  = ('F4-A1',  'F4-M1')
    EEG_5  = ('C4-A1',  'C4-M1')
    EEG_5  = ('O2-A1',  'O2-M1')
    CHIN   = ('X1','24',)
    ECG    = ('X2','25',)
    LEG_1  = ('X3','26',)
    LEG_2  = ('X4','27',)
    SNORE  = ('X5','28',)
    FLOW_1 = ('X6','29',)
    FLOW_2 = ('DC3', 'DC4', 'DC01')
    ABDO_1 = ('X7','30')
    ABDO_2 = ('X8','31')
    OXYMET = ('SaO2','SpO2')
    BODPOS = ('DC8',)
    #channels = [*EEG_2,*EEG_5,*EEG_3,*EOG_L]
    #channels = [*EEG_2,*EOG_L]
    channels = [*EOG_L]
elif active_dataset == Sources.edf_epoch_multichannel: #TODO FILL
    EOG    = ('EOG horizontal',)
    EEG_1  = ('EEG Fpz-Cz',)
    EEG_2  = ('EEG Pz-Oz',)
    RESP   = ('Resp oro-nasal',)
    EMG    = ('EMG submental',)
    TEMP   = ('Temp rectal',)
    channels = [*EEG_2,*EOG]
else:
    raise ValueError("Channels not defined for source {}!".format(active_dataset))

## Database settings
# ISRUC settings
# Calculate recording ids
import random
if active_dataset == Sources.isruc_epoch_multichannel:
    database = Path("./database")
    max_ids = 30
    subgroups = {1 : "I", 2 : "II", 3 : "III" , 0 : "*"}
    subgroup = subgroups[3]
    rec_ids = [str(f) for f in Path(database).glob("*{}/*".format(subgroup))]
    rec_ids = random.sample(rec_ids,min(max_ids,len(rec_ids)))
elif active_dataset == Sources.edf_epoch_multichannel:
    database = Path("./sleep-edf")
    max_ids_group = 10
    rec_ids = list()
    subgroups = {"SC" : "sleep-cassette", "ST" : "sleep-telemetry"}
    for subdir in subgroups.values():
        files = Path(database).glob("*{}/*".format(subdir))
        files = ["".join(str(f).split("-Hypnogram")[:-1]) for f in files]
        files = [f for f in files if len(f) > 0]
        # Sleep-EDF files have two letters for denoting the scorer,
        # That info does is not needed
        files = [f[:-2] for f in files]
        files = random.sample(files,max_ids_group)
        rec_ids.extend(files)
else:
    raise ValueError("Dataset ids not defined for {}!".format(str(active_dataset_name)))

# Dataset name
if active_dataset == Sources.isruc_epoch_multichannel:
    active_dataset_name = "ISRUC on {} group {}".format(str(channels), subgroup)
elif active_dataset == Sources.edf_epoch_multichannel:
    active_dataset_name = "Sleep-edf on {}".format(str(channels))
else:
    active_dataset_name= str(active_dataset)

QUICK = False
if QUICK:
    rec_ids = rec_ids[0:2]

## Dataset settings
# split settings (fracs sum to 1.0)
frac_train = 0.95
frac_val   = 0.05
# context adding window (>=0, 0 = no additional context)
context_left = 1
context_right = 1
# wavelet length for wavelet-based methods
target_len = 128

## Model settings
# active model (config below)
active_models = [
    #models.Experiment,  # Top val_accuracy ?
    #models.Densenet2D,    # Top val_accuracy ?
    #models.SimpleCNN2D,   # Top val_accuracy ?
    #models.RandomForest,  # Top val_accuracy ?
    #models.Densenet15D,   # Top val_accuracy 75% epoch 8
    #models.Densenet15DHD,    # Top val_accuracy 69% epoch 10 densenet15D
    #models.Densenet15DHDFFT,  # Top val_accuracy ?
    #models.Densenet15DHDDWT,  # Top val_accuracy ?
    #models.SpecializedDensenet1D,  # Top val_accuracy ?
    #models.SpecializedDensenet1DHD,  # Top val_accuracy ?
    #models.SpecializedDensenet1DHDFFT,  # Top val_accuracy ?
    ##models.HDModel,       # Top val_accuracy ?
    #models.DecisionTree,  # Top val_accuracy ?
    #models.SpecializedSpecializedDensenet1D,  # Top val_accuracy ?
    models.SpecializedDensenet15D,  # Top val_accuracy ?
    models.SpecializedDensenet15DHD,  # Top val_accuracy ?
    #models.SpecializedDensenet15DHDFFT,  # Top val_accuracy ?
    #models.SpecializedDensenet15DHDDWT,  # Top val_accuracy ?
    #models.TsinalisCNN, # Top val_accuracy ~73% epoch 4
    #models.TsinalisFFT,  # Top val_accuracy ?
    #models.RahmanForestDWT,  # Top val_accuracy ?
]

if QUICK:
    active_models = active_models[0:2]

#scalerConstructor =  sklearn.preprocessing.RobustScaler
#scalerConstructor =  sklearn.preprocessing.StandardScaler
scalerConstructor = namedtuple("NoScaler", "fit, transform", defaults=[lambda x:x, lambda x:x])

def channel_scaler_factory(): return scalerConstructor()
def active_scaler(n,fac): return models.ChannelScaler(n, channel_scaler_factory)

# keras dictionary
keras_fit_kwargs = {
    'batch_size' : 4, # Mini batch  see 2018 REVISITING SMALL BATCH TRAINING FOR DEEP NEURAL NETWORKS
    'epochs' : 32,
    'validation_split' : 0,
    'shuffle' : True,
    'verbose' : 1,
    #'callbacks' : callbacks in build_model
}
#if QUICK: keras_fit_kwargs['epochs']=2

# Densenet settings
densenet_kwargs = {
    'growth_rate' : 32,
    'n_dense_blocks' : 2,
    'n_layers_block' : 4,
}

sp_densenet_kwargs = {
    'growth_rate' : 8,
    'n_dense_blocks' : 4,
    'n_layers_block' : 4,
    'n_branches'     : 64,
}

# ANNHD settings
annhd_kwargs = {
    'layer_name' : 'target'
}

tree_init_args = {}
tree_fit_args = {}
tree_search_args = {
    "n_iter" : 5,
    "cv"     : 5,
    #"n_jobs" : 2,
    "param_distributions" : {
        #"min_samples_leaf": scipy.stats.uniform(loc=0.0001,scale=0.0005-0.0001)
    }
}
forest_init_args = {
    "n_estimators" : 200,
    #"n_jobs" : 1,
}
forest_fit_args = {
    #"n_estimators" : 200,
    #"n_jobs" : 1,
}

from sklearn.model_selection import train_test_split
def shuffle_split(x,y,frac_val):
    if frac_val == 0: return x,y,np.array([]),np.array([])
    x, x_val, y, y_val = train_test_split(x,y,test_size=frac_val)
    return x, y, x_val, y_val

def build_model(x_shape, classes, active_model):
    """Builds a keras model compatible with the input and output shapes."""
    a = active_model

    if active_model == models.TsinalisCNN:
        m = a(x_shape, classes, keras_fit_kwargs)
    elif active_model == models.Experiment:
        m = a(x_shape, classes, keras_fit_kwargs)
    elif active_model == models.TsinalisFFT:
        m = a(x_shape, classes, keras_fit_kwargs)
    elif active_model == models.SimpleCNN2D:
        m = a(x_shape, classes, keras_fit_kwargs)
    elif active_model == models.HDModel:
        m = a(x_shape, classes)
    elif active_model == models.SpecializedDensenet1D:
        m = a(x_shape, classes, keras_fit_kwargs, **sp_densenet_kwargs)
    elif active_model == models.SpecializedDensenet1DHD:
        m = a(x_shape, classes, sp_densenet_kwargs, keras_fit_kwargs, **annhd_kwargs)
    elif active_model == models.SpecializedDensenet1DHDFFT:
        m = a(x_shape, classes, sp_densenet_kwargs, keras_fit_kwargs, **annhd_kwargs)
    elif active_model == models.Densenet15D:
        m = a(x_shape, classes, keras_fit_kwargs, **densenet_kwargs)
    elif active_model == models.Densenet15DHD:
        m = a(x_shape, classes, densenet_kwargs, keras_fit_kwargs, **annhd_kwargs)
    elif active_model == models.Densenet15DHDFFT:
        m = a(x_shape, classes, densenet_kwargs, keras_fit_kwargs, **annhd_kwargs)
    elif active_model == models.Densenet2D:
        m = a(x_shape, classes, keras_fit_kwargs, **densenet_kwargs)
    elif active_model == models.DecisionTree:
        m = a(init_kwargs = tree_init_args, fit_kwargs = tree_fit_args,
                search_kwargs = tree_search_args)
    elif active_model == models.RandomForest:
        m = a(init_kwargs = forest_init_args, fit_kwargs = forest_fit_args)
    elif active_model == models.SpecializedDensenet15D:
        m = a(x_shape, classes, keras_fit_kwargs, **sp_densenet_kwargs)
    elif active_model == models.SpecializedDensenet15DHD:
        m = a(x_shape, classes, sp_densenet_kwargs, keras_fit_kwargs, **annhd_kwargs)
    elif active_model == models.SpecializedDensenet15DHDFFT:
        m = a(x_shape, classes, sp_densenet_kwargs, keras_fit_kwargs, **annhd_kwargs)
    elif active_model == models.RahmanForestDWT:
        m = a(init_kwargs = forest_init_args, fit_kwargs = forest_fit_args)
    elif active_model == models.Densenet15DHDDWT:
        m = a(x_shape, classes, densenet_kwargs, keras_fit_kwargs, **annhd_kwargs)
    elif active_model == models.SpecializedDensenet15DHDDWT:
        m = a(x_shape, classes, sp_densenet_kwargs, keras_fit_kwargs, **annhd_kwargs)
    else:
        raise ValueError("No inititialization defined for {}!".format(str(active_model)))
    return m

import transforms.datasets.epoch_wavelets
import transforms.datasets.epoch_multichannel
import transforms.datasets.isruc_epoch_multichannel_timed
def build_dataset(rec_ids, multi_hypnogram=False):
    """Returns numpy arrays using the dataset selection setting,

    rec_ids: recording ids from the ISRUC dataset
    (e.g. [./database/subgroupIII/1, ...]

    Returns: samples[], stages[]"""
    if len(rec_ids) == 0: return np.array([])
    if active_dataset == Sources.isruc_epoch_wavelets:
        params = [target_len]
        f = transforms.datasets.epoch_wavelets.build_dataset
    elif active_dataset == Sources.isruc_epoch_multichannel:
        params = [channels, active_dataset, multi_hypnogram]
        f = transforms.datasets.epoch_multichannel.build_dataset
    elif active_dataset == Sources.edf_epoch_multichannel:
        params = [channels, active_dataset, multi_hypnogram]
        f = transforms.datasets.epoch_multichannel.build_dataset
    elif active_dataset == Sources.isruc_epoch_multichannel_timed:
        params = [channels, multi_hypnogram]
        f = transforms.datasets.isruc_epoch_multichannel_timed
    else:
        raise ValueError("Dataset building not defined for source {}".format(active_dataset))

    # Build each record's dataset (multithread)
    inputs = [[rec_id, *params] for rec_id in rec_ids]
    from itertools import starmap
    from itertools import chain
    #with Pool() as p: rec_datasets = p.starmap(f, inputs, chunksize=10)
    rec_datasets = starmap(f, inputs)
    # Build complete contextualized dataset
    samples = list()
    stages = list()
    shapes = set()
    for rec_samples, rec_stages in tqdm(rec_datasets,
            desc="Contextualizing datasets",
            total = len(inputs)):
        if multi_hypnogram: rec_stages = np.array(rec_stages).T
        for i,rec_stage in enumerate(rec_stages):
            j_left = i-context_left
            j_right = i+context_right+1
            if j_left >= 0 and j_right <= len(rec_samples):
                contextualized = np.concatenate(rec_samples[j_left:j_right])
                if shapes == set() or contextualized.shape in shapes:
                    shapes.add(contextualized.shape)
                    samples.append(contextualized)
                    stages.append(rec_stage)
                else:
                    print("Warning! Ignoring sample with shape {}!".format(
                        contextualized.shape))

    assert len(samples) == len(stages)
    assert len(set([len(sample) for sample in samples])) == 1
    samps = np.array(samples)
    stages = np.array(stages)
    if multi_hypnogram: stages = np.array(stages).T
    ###TEMP
    #for index in np.ndindex(stages.shape):
    #    if stages[index]==1:
    #        stages[index]=1
    #    else:
    #        stages[index]=0
    ###
    return samps, stages
